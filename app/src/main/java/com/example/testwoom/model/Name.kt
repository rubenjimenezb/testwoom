package com.example.testwoom.model

import android.os.Parcel
import android.os.Parcelable
import javax.annotation.Generated
import com.squareup.moshi.Json

@Generated("com.robohorse.robopojogenerator")
data class Name(

	@Json(name="last")
	val last: String? = null,

	@Json(name="title")
	val title: String? = null,

	@Json(name="first")
	val first: String? = null
) : Parcelable {
	constructor(parcel: Parcel) : this(
		parcel.readString(),
		parcel.readString(),
		parcel.readString()
	)

	fun getFullName(): String {
		return "$title $first $last"
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeString(last)
		parcel.writeString(title)
		parcel.writeString(first)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<Name> {
		override fun createFromParcel(parcel: Parcel): Name {
			return Name(parcel)
		}

		override fun newArray(size: Int): Array<Name?> {
			return arrayOfNulls(size)
		}
	}
}