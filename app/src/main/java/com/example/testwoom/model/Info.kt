package com.example.testwoom.model

import android.os.Parcel
import android.os.Parcelable
import javax.annotation.Generated
import com.squareup.moshi.Json

@Generated("com.robohorse.robopojogenerator")
data class Info(

	@Json(name="seed")
	val seed: String? = null,

	@Json(name="page")
	val page: Int? = null,

	@Json(name="results")
	val results: Int? = null,

	@Json(name="version")
	val version: String? = null
) : Parcelable {
	constructor(parcel: Parcel) : this(
		parcel.readString(),
		parcel.readValue(Int::class.java.classLoader) as? Int,
		parcel.readValue(Int::class.java.classLoader) as? Int,
		parcel.readString()
	) {
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeString(seed)
		parcel.writeValue(page)
		parcel.writeValue(results)
		parcel.writeString(version)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<Info> {
		override fun createFromParcel(parcel: Parcel): Info {
			return Info(parcel)
		}

		override fun newArray(size: Int): Array<Info?> {
			return arrayOfNulls(size)
		}
	}
}