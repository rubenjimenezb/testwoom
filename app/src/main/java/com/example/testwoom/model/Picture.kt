package com.example.testwoom.model

import android.os.Parcel
import android.os.Parcelable
import javax.annotation.Generated
import com.squareup.moshi.Json

@Generated("com.robohorse.robopojogenerator")
data class Picture(

	@Json(name="thumbnail")
	val thumbnail: String? = null,

	@Json(name="large")
	val large: String? = null,

	@Json(name="medium")
	val medium: String? = null
) : Parcelable {
	constructor(parcel: Parcel) : this(
		parcel.readString(),
		parcel.readString(),
		parcel.readString()
	)

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeString(thumbnail)
		parcel.writeString(large)
		parcel.writeString(medium)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<Picture> {
		override fun createFromParcel(parcel: Parcel): Picture {
			return Picture(parcel)
		}

		override fun newArray(size: Int): Array<Picture?> {
			return arrayOfNulls(size)
		}
	}
}