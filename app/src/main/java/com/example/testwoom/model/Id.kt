package com.example.testwoom.model

import android.os.Parcel
import android.os.Parcelable
import javax.annotation.Generated
import com.squareup.moshi.Json

@Generated("com.robohorse.robopojogenerator")
data class Id(

	@Json(name="name")
	val name: String? = null,

	@Json(name="value")
	val value: String? = null
) : Parcelable {
	constructor(parcel: Parcel) : this(
		parcel.readString(),
		parcel.readString()
	)

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeString(name)
		parcel.writeString(value)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<Id> {
		override fun createFromParcel(parcel: Parcel): Id {
			return Id(parcel)
		}

		override fun newArray(size: Int): Array<Id?> {
			return arrayOfNulls(size)
		}
	}
}