package com.example.testwoom.model

import android.os.Parcel
import android.os.Parcelable
import javax.annotation.Generated
import com.squareup.moshi.Json

@Generated("com.robohorse.robopojogenerator")
data class Location(

	@Json(name="city")
	val city: String? = null,

	@Json(name="street")
	val street: String? = null,

	@Json(name="timezone")
	val timezone: Timezone? = null,

	@Json(name="coordinates")
	val coordinates: Coordinates? = null,

	@Json(name="state")
	val state: String? = null
) : Parcelable {
	constructor(parcel: Parcel) : this(
		parcel.readString(),
		parcel.readString(),
		parcel.readParcelable(Timezone::class.java.classLoader),
		parcel.readParcelable(Coordinates::class.java.classLoader),
		parcel.readString()
	)

	fun getFullLocation(): String {
		return "$street, $city ($state)"
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeString(city)
		parcel.writeString(street)
		parcel.writeParcelable(timezone, flags)
		parcel.writeParcelable(coordinates, flags)
		parcel.writeString(state)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<Location> {
		override fun createFromParcel(parcel: Parcel): Location {
			return Location(parcel)
		}

		override fun newArray(size: Int): Array<Location?> {
			return arrayOfNulls(size)
		}
	}
}