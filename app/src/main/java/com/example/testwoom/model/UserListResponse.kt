package com.example.testwoom.model

import javax.annotation.Generated
import com.squareup.moshi.Json

@Generated("com.robohorse.robopojogenerator")
data class UserListResponse(

	@Json(name="results")
	val results: List<ResultsItem>? = null,

	@Json(name="info")
	val info: Info? = null
)