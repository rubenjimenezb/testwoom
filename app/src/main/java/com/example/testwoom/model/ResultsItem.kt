package com.example.testwoom.model

import android.os.Parcel
import android.os.Parcelable
import javax.annotation.Generated
import com.squareup.moshi.Json

@Generated("com.robohorse.robopojogenerator")
data class ResultsItem(

	@Json(name="nat")
	val nat: String? = null,

	@Json(name="gender")
	val gender: String? = null,

	@Json(name="phone")
	val phone: String? = null,

	@Json(name="dob")
	val dob: Dob? = null,

	@Json(name="name")
	val name: Name? = null,

	@Json(name="registered")
	val registered: Registered? = null,

	@Json(name="location")
	val location: Location? = null,

	@Json(name="id")
	val id: Id? = null,

	@Json(name="login")
	val login: Login? = null,

	@Json(name="cell")
	val cell: String? = null,

	@Json(name="email")
	val email: String? = null,

	@Json(name="picture")
	val picture: Picture? = null
) : Parcelable {
	constructor(parcel: Parcel) : this(
		parcel.readString(),
		parcel.readString(),
		parcel.readString(),
		parcel.readParcelable(Dob::class.java.classLoader),
		parcel.readParcelable(Name::class.java.classLoader),
		parcel.readParcelable(Registered::class.java.classLoader),
		parcel.readParcelable(Location::class.java.classLoader),
		parcel.readParcelable(Id::class.java.classLoader),
		parcel.readParcelable(Login::class.java.classLoader),
		parcel.readString(),
		parcel.readString(),
		parcel.readParcelable(Picture::class.java.classLoader)
	)

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeString(nat)
		parcel.writeString(gender)
		parcel.writeString(phone)
		parcel.writeParcelable(dob, flags)
		parcel.writeParcelable(name, flags)
		parcel.writeParcelable(registered, flags)
		parcel.writeParcelable(location, flags)
		parcel.writeParcelable(id, flags)
		parcel.writeParcelable(login, flags)
		parcel.writeString(cell)
		parcel.writeString(email)
		parcel.writeParcelable(picture, flags)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<ResultsItem> {
		override fun createFromParcel(parcel: Parcel): ResultsItem {
			return ResultsItem(parcel)
		}

		override fun newArray(size: Int): Array<ResultsItem?> {
			return arrayOfNulls(size)
		}
	}
}