package com.example.testwoom.model

import android.os.Parcel
import android.os.Parcelable
import javax.annotation.Generated
import com.squareup.moshi.Json

@Generated("com.robohorse.robopojogenerator")
data class Login(

	@Json(name="sha1")
	val sha1: String? = null,

	@Json(name="password")
	val password: String? = null,

	@Json(name="salt")
	val salt: String? = null,

	@Json(name="sha256")
	val sha256: String? = null,

	@Json(name="uuid")
	val uuid: String? = null,

	@Json(name="username")
	val username: String? = null,

	@Json(name="md5")
	val md5: String? = null
) : Parcelable {
	constructor(parcel: Parcel) : this(
		parcel.readString(),
		parcel.readString(),
		parcel.readString(),
		parcel.readString(),
		parcel.readString(),
		parcel.readString(),
		parcel.readString()
	)

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeString(sha1)
		parcel.writeString(password)
		parcel.writeString(salt)
		parcel.writeString(sha256)
		parcel.writeString(uuid)
		parcel.writeString(username)
		parcel.writeString(md5)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<Login> {
		override fun createFromParcel(parcel: Parcel): Login {
			return Login(parcel)
		}

		override fun newArray(size: Int): Array<Login?> {
			return arrayOfNulls(size)
		}
	}
}