package com.example.testwoom.model

import android.os.Parcel
import android.os.Parcelable
import javax.annotation.Generated
import com.squareup.moshi.Json

@Generated("com.robohorse.robopojogenerator")
data class Dob(

	@Json(name="date")
	val date: String? = null,

	@Json(name="age")
	val age: Int? = null
) : Parcelable {
	constructor(parcel: Parcel) : this(
		parcel.readString(),
		parcel.readValue(Int::class.java.classLoader) as? Int
	)

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeString(date)
		parcel.writeValue(age)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<Dob> {
		override fun createFromParcel(parcel: Parcel): Dob {
			return Dob(parcel)
		}

		override fun newArray(size: Int): Array<Dob?> {
			return arrayOfNulls(size)
		}
	}
}