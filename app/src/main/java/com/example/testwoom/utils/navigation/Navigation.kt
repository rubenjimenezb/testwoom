package com.example.testwoom.utils.navigation

import android.content.Context
import android.view.View
import com.example.testwoom.model.ResultsItem

interface Navigation {
    fun goUserDetailScreen(context: Context, userDetail: ResultsItem, viewToShare: View, shareKey: String)
}