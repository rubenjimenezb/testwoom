package com.example.testwoom.utils.navigation

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Build
import android.view.View
import androidx.core.app.ActivityOptionsCompat
import com.example.testwoom.model.ResultsItem
import com.example.testwoom.ui.detail.UserDetailActivity

class NavigationImpl : Navigation {
    override fun goUserDetailScreen(context: Context, userDetail: ResultsItem, viewToShare: View, shareKey: String) {
        //Go to user detail screen
        val intent = Intent(context, UserDetailActivity::class.java)
        intent.putExtra(NavigationConstants.USER_ITEM_KEY, userDetail)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            val options = ActivityOptionsCompat.makeSceneTransitionAnimation((context as Activity?)!!, viewToShare, shareKey)
            context.startActivity(intent, options.toBundle())

        } else {
            context.startActivity(intent)
        }
    }
}