package com.example.testwoom.network

import com.example.testwoom.model.UserListResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * The interface which provides methods to get result of webservices
 */
interface UserApi {
    /**
     * Get the list of users from the API
     */
    @GET("/?results=10")
    fun getUsers(
        @Query("seed") seed: String = "abc",
        @Query("page") page: Int = 1
    ): Observable<UserListResponse>
}