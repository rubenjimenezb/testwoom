package com.example.testwoom.di.component

import com.example.testwoom.di.module.NetworkModule
import com.example.testwoom.ui.detail.UserDetailViewModel
import com.example.testwoom.ui.main.MainViewModel
import dagger.Component
import javax.inject.Singleton

/**
 * Component providing inject() methods for presenters.
 */
@Singleton
@Component(modules = [(NetworkModule::class)])
interface ViewModelInjector {
    /**
     * Injects required dependencies into the specified MainViewModel.
     * @param mainViewModel MainViewModel in which to inject the dependencies
     */
    fun inject(mainViewModel: MainViewModel)

    /**
     * Injects required dependencies into the specified UserDetailViewModel.
     * @param userDetailViewModel UserDetailViewModel in which to inject the dependencies
     */
    fun inject(userDetailViewModel: UserDetailViewModel)

    @Component.Builder
    interface Builder {
        fun build(): ViewModelInjector

        fun networkModule(networkModule: NetworkModule): Builder
    }
}