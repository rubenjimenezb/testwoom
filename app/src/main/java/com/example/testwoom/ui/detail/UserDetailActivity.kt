package com.example.testwoom.ui.detail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.example.testwoom.R
import com.example.testwoom.databinding.ActivityUserDetailBinding
import com.example.testwoom.model.ResultsItem
import com.example.testwoom.utils.navigation.NavigationConstants

class UserDetailActivity : AppCompatActivity() {

    private lateinit var binding: ActivityUserDetailBinding
    private lateinit var viewModel: UserDetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val user = intent.getParcelableExtra(NavigationConstants.USER_ITEM_KEY) as ResultsItem

        binding = DataBindingUtil.setContentView(this, R.layout.activity_user_detail)
        viewModel = ViewModelProviders.of(this).get(UserDetailViewModel::class.java)
        viewModel.setDetailUser(user)

        binding.viewModel = viewModel
    }
}
