package com.example.testwoom.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.testwoom.R
import com.example.testwoom.databinding.ItemUserFemaleBinding
import com.example.testwoom.databinding.ItemUserMaleBinding
import com.example.testwoom.model.ResultsItem

class UserListAdapter: RecyclerView.Adapter<UserListAdapter.ViewHolder>() {
    private var userList = emptyList<ResultsItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return when(viewType){
            FEMALE_VALUE -> {
                val binding: ItemUserFemaleBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_user_female, parent, false)
                ViewHolder(binding)
            }
            else -> {
                val binding: ItemUserMaleBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_user_male, parent, false)
                ViewHolder(binding)
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(userList[position])
    }

    override fun getItemCount(): Int {
        return userList.size

    }

    override fun getItemViewType(position: Int): Int {
        return if (userList[position].gender == FEMALE_KEY) FEMALE_VALUE else MALE_VALUE
    }

    /**
     * Update user list, add the new list to the existing list and removes duplicated items if necessary
     */
    fun updateUserList(userList:List<ResultsItem>){
        val partialList = ArrayList<ResultsItem>()
        partialList.addAll(this.userList)
        partialList.addAll(userList)

        this.userList = partialList.distinct()
        notifyDataSetChanged()
    }

    class ViewHolder :RecyclerView.ViewHolder{
        private val viewModel = UserViewModel()
        private lateinit var femaleBinding: ItemUserFemaleBinding
        private lateinit var maleBinding: ItemUserMaleBinding

        constructor(binding: ItemUserFemaleBinding) : super(binding.root){
            femaleBinding = binding
            femaleBinding.viewModel = viewModel
        }

        constructor(binding: ItemUserMaleBinding) : super(binding.root){
            maleBinding = binding
            maleBinding.viewModel = viewModel
        }

        fun bind(user:ResultsItem){
            viewModel.bind(user)
        }
    }

    companion object {
        const val FEMALE_KEY = "female"
        const val FEMALE_VALUE = 0
        const val MALE_VALUE = 1
    }
}