package com.example.testwoom.ui.main

import android.view.View
import androidx.lifecycle.MutableLiveData
import com.example.testwoom.R
import com.example.testwoom.base.BaseViewModel
import com.example.testwoom.model.ResultsItem
import com.example.testwoom.utils.navigation.NavigationImpl

class UserViewModel  : BaseViewModel(){
    private val userFullName = MutableLiveData<String>()
    private val userMail = MutableLiveData<String>()
    private val userPhone = MutableLiveData<String>()
    private val userUrlPhoto = MutableLiveData<String>()
    private lateinit var user : ResultsItem

    fun bind(user: ResultsItem){
        this.user = user
        userFullName.value = user.name?.getFullName() ?:""
        userMail.value = user.email
        userPhone.value = user.phone
        userUrlPhoto.value = user.picture?.medium ?: ""
    }

    fun getUserFullName():MutableLiveData<String>{
        return userFullName
    }

    fun getUserMail():MutableLiveData<String>{
        return userMail
    }

    fun getUserPhone():MutableLiveData<String>{
        return userPhone
    }

    fun getUserUrlPhoto():MutableLiveData<String>{
        return userUrlPhoto
    }

    fun onElementClicked(view: View){
        NavigationImpl().goUserDetailScreen(view.context, user, view.findViewById(R.id.ivUser), view.context.getString(R.string.user_share_image))
    }
}