package com.example.testwoom.ui.detail

import androidx.lifecycle.MutableLiveData
import com.example.testwoom.base.BaseViewModel
import com.example.testwoom.model.ResultsItem

class UserDetailViewModel : BaseViewModel() {

    private val userFullName = MutableLiveData<String>()
    private val userMail = MutableLiveData<String>()
    private val userGender = MutableLiveData<String>()
    private val userUrlPhoto = MutableLiveData<String>()
    private val userRegisteredDate = MutableLiveData<String>()
    private val userLocation = MutableLiveData<String>()
    private lateinit var user : ResultsItem

    fun setDetailUser(user: ResultsItem){
        this.user = user
        userFullName.value = user.name?.getFullName() ?: ""
        userMail.value = user.email
        userGender.value = user.gender
        userUrlPhoto.value = user.picture?.medium ?: ""
        userRegisteredDate.value = user.registered?.date ?: ""
        userLocation.value = user.location?.getFullLocation() ?: ""

    }

    fun getUserFullName():MutableLiveData<String>{
        return userFullName
    }

    fun getUserMail():MutableLiveData<String>{
        return userMail
    }

    fun getUserGender():MutableLiveData<String>{
        return userGender
    }

    fun getUserUrlPhoto():MutableLiveData<String>{
        return userUrlPhoto
    }

    fun getUserLocation():MutableLiveData<String>{
        return userLocation
    }

    fun getUserRegisteredDate():MutableLiveData<String>{
        return userRegisteredDate
    }
}