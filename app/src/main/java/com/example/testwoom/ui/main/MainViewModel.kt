package com.example.testwoom.ui.main

import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.example.testwoom.R
import com.example.testwoom.base.BaseViewModel
import com.example.testwoom.model.ResultsItem
import com.example.testwoom.model.UserListResponse
import com.example.testwoom.network.UserApi
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MainViewModel: BaseViewModel(){
    @Inject
    lateinit var userApi: UserApi

    private lateinit var subscription: Disposable

    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()

    val errorMessage:MutableLiveData<Int> = MutableLiveData()
    val errorClickListener = View.OnClickListener {
        if(nextPageCode != 0){
            loadUsers(nextPageCode)
        }
    }

    val userListAdapter : UserListAdapter = UserListAdapter()

    private var nextPageCode : Int = 1

    val recyclerViewOnScrollListener = object : RecyclerView.OnScrollListener(){
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)

            if (!recyclerView.canScrollVertically(1)) {
                if(nextPageCode != 0){
                    loadUsers(nextPageCode)
                }
            }
        }
    }


    init{
        loadUsers()
    }

    private fun loadUsers(nextPageCode : Int = 1){
        subscription = userApi.getUsers(page = nextPageCode)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { onRetrieveUserListStart() }
            .doOnTerminate { onRetrieveUserListFinish() }
            .subscribe(
                { result -> onRetrieveUserListOk(result) },
                { error -> onRetrieveUserListError(error) }
            )
    }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }

    private fun onRetrieveUserListStart() {
        loadingVisibility.value = View.VISIBLE
        errorMessage.value = null
    }

    private fun onRetrieveUserListFinish() {
        loadingVisibility.value = View.GONE
    }

    private fun onRetrieveUserListOk(result: UserListResponse?) {
        Log.d("MainViewModel", "onRetrieveUserListOk: $result")

        nextPageCode = result?.info?.page?.plus(1) ?: 0
        userListAdapter.updateUserList(result?.results ?: emptyList())
    }

    private fun onRetrieveUserListError(error: Throwable?) {
        errorMessage.value = R.string.user_error
        Log.d("MainViewModel", error.toString())
    }
}