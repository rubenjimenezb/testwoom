package com.example.testwoom.base

import androidx.lifecycle.ViewModel
import com.example.testwoom.di.component.DaggerViewModelInjector
import com.example.testwoom.di.component.ViewModelInjector
import com.example.testwoom.di.module.NetworkModule
import com.example.testwoom.ui.detail.UserDetailViewModel
import com.example.testwoom.ui.main.MainViewModel

abstract class BaseViewModel: ViewModel() {
    private val injector: ViewModelInjector = DaggerViewModelInjector
        .builder()
        .networkModule(NetworkModule)
        .build()

    init {
        inject()
    }

    /**
     * Injects the required dependencies
     */
    private fun inject() {
        when (this) {
            is MainViewModel -> injector.inject(this)
            is UserDetailViewModel -> injector.inject(this)
        }
    }
}